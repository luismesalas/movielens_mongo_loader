import argparse
import csv
import importlib.machinery
import logging
import os
import types
from datetime import datetime

from pymongo import MongoClient


class MovielensMongoLoader:
    def __init__(self, settings_path):
        loader = importlib.machinery.SourceFileLoader(settings_path, settings_path)
        self.__settings = types.ModuleType(loader.name)
        loader.exec_module(self.__settings)

    def run_mongo_loader(self, movielens_files, mongo_uri):
        try:
            logging.info("Running Movielens MongoDB loader...")

            # Parameters and some configuration checks
            if not self.files_required_exists(movielens_files):
                raise Exception("Some required files are missing. "
                                "Check that you have all these files on your folder: {}"
                                .format(self.__settings.MOVIELENS_REQUIRED_FILES))

            if not mongo_uri:
                mongo_uri = self.__settings.MONGODB_LOCALHOST_URI

            logging.info("Stablishing connection to {}".format(mongo_uri))
            mongo_client = MongoClient(mongo_uri)
            movielens_db = mongo_client[self.__settings.MONGODB_DB_MOVIELENS]

            logging.info("Processing Genome Scores file...")
            genome_scores_collection = movielens_db[self.__settings.MONGODB_GENOME_SCORES_COLLECTION]

            genome_scores_file = open(os.path.join(movielens_files, self.__settings.GENOME_SCORES_FILE), 'r')
            genome_scores_file_reader = csv.DictReader(genome_scores_file)

            for genome_score in genome_scores_file_reader:
                genome_scores_collection.save(genome_score)

            logging.info(
                "Genome Scores stored in {} collection.".format(self.__settings.MONGODB_GENOME_SCORES_COLLECTION))

            logging.info("Processing Genome Tags file...")
            genome_tags_collection = movielens_db[self.__settings.MONGODB_GENOME_TAGS_COLLECTION]

            genome_tags_file = open(os.path.join(movielens_files, self.__settings.GENOME_TAGS_FILE), 'r')
            genome_tags_file_reader = csv.DictReader(genome_tags_file)

            for genome_tag in genome_tags_file_reader:
                genome_tags_collection.save(genome_tag)
            logging.info("Genome Tags stored in {} collection.".format(self.__settings.MONGODB_GENOME_TAGS_COLLECTION))

            logging.info("Processing Links file...")
            links_collection = movielens_db[self.__settings.MONGODB_LINKS_COLLECTION]

            links_file = open(os.path.join(movielens_files, self.__settings.LINKS_FILE), 'r')
            links_file_reader = csv.DictReader(links_file)

            for link in links_file_reader:
                links_collection.save(link)
            logging.info("Links stored in {} collection.".format(self.__settings.MONGODB_LINKS_COLLECTION))

            logging.info("Processing Movies file...")
            movies_collection = movielens_db[self.__settings.MONGODB_MOVIES_COLLECTION]

            movies_file = open(os.path.join(movielens_files, self.__settings.MOVIES_FILE), 'r')
            movies_file_reader = csv.DictReader(movies_file)

            for movie in movies_file_reader:
                # genres field in movies items is a pipe-separated field, we will transform it to an array
                movie['genres'] = movie['genres'].split('|')
                movies_collection.save(movie)
            logging.info("Movies stored in {} collection.".format(self.__settings.MONGODB_MOVIES_COLLECTION))

            logging.info("Processing Ratings file...")
            ratings_collection = movielens_db[self.__settings.MONGODB_RATINGS_COLLECTION]

            ratings_file = open(os.path.join(movielens_files, self.__settings.RATINGS_FILE), 'r')
            ratings_file_reader = csv.DictReader(ratings_file)

            for rating in ratings_file_reader:
                ratings_collection.save(rating)
            logging.info("Ratings stored in {} collection.".format(self.__settings.MONGODB_RATINGS_COLLECTION))

            logging.info("Processing Tags file...")
            tags_collection = movielens_db[self.__settings.MONGODB_TAGS_COLLECTION]

            tags_file = open(os.path.join(movielens_files, self.__settings.TAGS_FILE), 'r')
            tags_file_reader = csv.DictReader(tags_file)

            for tag in tags_file_reader:
                tags_collection.save(tag)
            logging.info("Tags stored in {} collection.".format(self.__settings.MONGODB_TAGS_COLLECTION))


        except Exception as ex:
            logging.exception("[Movielens MongoDB loader] Exception: {}".format(ex.args))
            exit(-1)

    def files_required_exists(self, path):
        for file in self.__settings.MOVIELENS_REQUIRED_FILES:
            if not os.path.exists(os.path.join(path, file)):
                return False
        return True


def main():
    loader = importlib.machinery.SourceFileLoader("config/settings.py", "config/settings.py")
    settings = types.ModuleType(loader.name)
    loader.exec_module(settings)
    logging.basicConfig(filename=settings.LOG_PATH + str(datetime.today()), level=logging.INFO,
                        format=settings.LOG_FORMAT)

    parser = argparse.ArgumentParser(description='Movielens MongoDB loader')
    parser.add_argument('--movielens_files', type=valid_dataset, help='Folder path to movielens csv files.',
                        required=True)
    parser.add_argument('--mongo_uri', help='URI to MongoDB Service. Localhost by default.', required=False)

    args = parser.parse_args()
    logging.info("Params <{}>".format(args))
    logging.info("Starting movielens load process")
    try:
        movielens_mongo_loader = MovielensMongoLoader("config/settings.py")
        movielens_mongo_loader.run_mongo_loader(args.movielens_files, args.mongo_uri)
    except Exception as ex_main:
        logging.error("ERROR: An error raised and the process ended: {}".format(str(ex_main)))
        exit(-1)

    logging.info("Load process finished!")


def valid_dataset(d):
    try:
        if os.path.isdir(d):
            return d
        else:
            msg = "Not a valid folder: '{0}'.".format(d)
            raise argparse.ArgumentTypeError(msg)
    except ValueError:
        msg = "Not a valid folder: '{0}'.".format(d)
        raise argparse.ArgumentTypeError(msg)


if __name__ == '__main__':
    main()
