import argparse
import importlib.machinery
import logging
import re
import types
from datetime import datetime

import unidecode
from pymongo import MongoClient


class MoviesFlatter:
    def __init__(self, settings_path):
        loader = importlib.machinery.SourceFileLoader(settings_path, settings_path)
        self.__settings = types.ModuleType(loader.name)
        loader.exec_module(self.__settings)

    def run_movies_flatter(self, mongo_uri):
        try:
            logging.info("Running Movies flatter...")

            if not mongo_uri:
                mongo_uri = self.__settings.MONGODB_LOCALHOST_URI

            logging.info("Stablishing connection to {}".format(mongo_uri))
            mongo_client = MongoClient(mongo_uri)
            movielens_db = mongo_client[self.__settings.MONGODB_DB_MOVIELENS]

            logging.info("Processing TMDB movies for flattenning...")
            tmdb_movies_collection = movielens_db[self.__settings.MONGODB_TMDB_MOVIES_COLLECTION]
            flatten_movies_collection = movielens_db[self.__settings.MONGODB_FLATTEN_MOVIES_COLLECTION]

            for movie in tmdb_movies_collection.find():
                movie_id = movie['movieId']
                title = movie['title']
                genres = movie['genres']
                cast = movie['cast']
                crew = movie['crew']

                normalized_terms = []

                for genre in genres:
                    normalized_terms.append(MoviesFlatter.normalize_text('genre_{}'.format(genre['name'])))

                for cast_member in cast:
                    normalized_terms.append(MoviesFlatter.normalize_text('actor_{}'.format(cast_member['name'])))

                for crew_member in crew:
                    if crew_member['job'] == 'Director':
                        normalized_terms.append(MoviesFlatter.normalize_text('director_{}'.format(crew_member['name'])))
                    if crew_member['job'] == 'Producer':
                        normalized_terms.append(MoviesFlatter.normalize_text('producer_{}'.format(crew_member['name'])))
                    if crew_member['job'] == 'Executive Producer':
                        normalized_terms.append(
                            MoviesFlatter.normalize_text('executive_producer_{}'.format(crew_member['name'])))
                    if crew_member['job'] == 'Editor':
                        normalized_terms.append(MoviesFlatter.normalize_text('editor_{}'.format(crew_member['name'])))
                    if crew_member['job'] == 'Writer':
                        normalized_terms.append(MoviesFlatter.normalize_text('writer_{}'.format(crew_member['name'])))

                # We only add movies with terms
                if normalized_terms:
                    flatten_movies_collection.save({'movieId': movie_id, 'title': title, 'terms': normalized_terms})

            logging.info("Movies processed")

        except Exception as ex:
            logging.exception("[Movies flatter] Exception: {}".format(ex.args))
            exit(-1)

    def normalize_text(text):
        text = unidecode.unidecode(text)
        text = text.lower()
        text = text.replace(' ', '_')
        text = text.replace('jr.', 'jr')
        text = text.replace('sr.', 'sr')
        text = text.replace('.', '_')
        text = text.replace(',', '_')
        text = text.replace('-', '_')
        text = text.replace(':', '_')
        text = text.replace(';', '_')
        text = text.replace('(', '_')
        text = text.replace(')', '_')
        text = text.replace('!', '_')
        text = text.replace('?', '_')
        text = text.replace('@', '_')
        text = text.replace('$', '_')
        text = text.replace('~', '_')
        text = text.replace('\t', '_')
        text = text.replace('\\', '_')
        text = text.replace('/', '_')
        text = text.replace('\'', '_')
        text = text.replace('"', '_')
        text = text.replace('`', '_')
        text = re.sub('_+', '_', text)
        return text


def main():
    loader = importlib.machinery.SourceFileLoader("config/settings.py", "config/settings.py")
    settings = types.ModuleType(loader.name)
    loader.exec_module(settings)
    logging.basicConfig(filename=settings.LOG_PATH + str(datetime.today()), level=logging.INFO,
                        format=settings.LOG_FORMAT)

    parser = argparse.ArgumentParser(description='Movies flatter')
    parser.add_argument('--mongo_uri', help='URI to MongoDB Service. Localhost by default.', required=False)

    args = parser.parse_args()
    logging.info("Params <{}>".format(args))
    logging.info("Starting movies flatter")
    try:
        movies_flatter = MoviesFlatter("config/settings.py")
        movies_flatter.run_movies_flatter(args.mongo_uri)
    except Exception as ex_main:
        logging.error("ERROR: An error raised and the process ended: {}".format(str(ex_main)))
        exit(-1)

    logging.info("Flatten process finished!")


if __name__ == '__main__':
    main()
