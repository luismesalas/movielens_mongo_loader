import argparse
import importlib.machinery
import json
import logging
import os
import types
from datetime import datetime

from pymongo import MongoClient


class MovielensVsTMDBLoader:
    def __init__(self, settings_path):
        loader = importlib.machinery.SourceFileLoader(settings_path, settings_path)
        self.__settings = types.ModuleType(loader.name)
        loader.exec_module(self.__settings)

    def run_tmdb_loader(self, tmdb_files, mongo_uri):
        try:
            logging.info("Running Movielens MongoDB loader...")

            if not mongo_uri:
                mongo_uri = self.__settings.MONGODB_LOCALHOST_URI

            logging.info("Stablishing c onnection to {}".format(mongo_uri))
            mongo_client = MongoClient(mongo_uri)
            movielens_db = mongo_client[self.__settings.MONGODB_DB_MOVIELENS]

            logging.info("Processing TMDB files...")
            links_collection = movielens_db[self.__settings.MONGODB_LINKS_COLLECTION]
            tmdb_movies_collection = movielens_db[self.__settings.MONGODB_TMDB_MOVIES_COLLECTION]

            for link in links_collection.find():
                movie_id = link['movieId']
                tmdb_id = link['tmdbId']

                tmdb_file_path = os.path.join(tmdb_files, self.__settings.TMDB_FILE_PATTERN.format(tmdb_id))
                if os.path.exists(tmdb_file_path):
                    tmdb_movie = json.load(open(tmdb_file_path, 'r'))
                    # 'tmdbId' is represented by 'id' field, we append 'movieId' for matching with movielens
                    tmdb_movie.update({'movieId': movie_id})
                    tmdb_movies_collection.save(tmdb_movie)
                else:
                    logging.warning(
                        "File not found for movieId '{}', expected file: {}.".format(movie_id, tmdb_file_path))

            logging.info("TMDB files processed")

        except Exception as ex:
            logging.exception("[Movielens vs TMDB loader] Exception: {}".format(ex.args))
            exit(-1)


def main():
    loader = importlib.machinery.SourceFileLoader("config/settings.py", "config/settings.py")
    settings = types.ModuleType(loader.name)
    loader.exec_module(settings)
    logging.basicConfig(filename=settings.LOG_PATH + str(datetime.today()), level=logging.INFO,
                        format=settings.LOG_FORMAT)

    parser = argparse.ArgumentParser(description='TMDB MongoDB loader')
    parser.add_argument('--tmdb_files', type=valid_dataset, help='Folder path to TMDB Json files.',
                        required=True)
    parser.add_argument('--mongo_uri', help='URI to MongoDB Service. Localhost by default.', required=False)

    args = parser.parse_args()
    logging.info("Params <{}>".format(args))
    logging.info("Starting tmdb load process")
    try:
        movielens_vs_tmdb_loader = MovielensVsTMDBLoader("config/settings.py")
        movielens_vs_tmdb_loader.run_tmdb_loader(args.tmdb_files, args.mongo_uri)
    except Exception as ex_main:
        logging.error("ERROR: An error raised and the process ended: {}".format(str(ex_main)))
        exit(-1)

    logging.info("Load process finished!")


def valid_dataset(d):
    try:
        if os.path.isdir(d):
            return d
        else:
            msg = "Not a valid folder: '{0}'.".format(d)
            raise argparse.ArgumentTypeError(msg)
    except ValueError:
        msg = "Not a valid folder: '{0}'.".format(d)
        raise argparse.ArgumentTypeError(msg)


if __name__ == '__main__':
    main()
